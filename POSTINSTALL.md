First start after install will take some time as the database is imported for first-time use!

Default login is:

USERNAME: `admin@example.com`
PASSWORD: `changeme`

**BE SURE TO LOGIN AND CHANGE THESE RIGHT AWAY!**


SFTP Access:

**Host**: $CLOUDRON-API-DOMAIN<br/>
**Port**: 222<br/>
**Username**: $CLOUDRON-USERNAME@$CLOUDRON-APP-DOMAIN<br/>
**Password**: Your Cloudron Password<br/>

