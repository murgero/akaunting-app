#!/bin/bash

## Build app
docker build -t mitchellurgero/org.urgero.akaunting:latest . --no-cache 
docker push mitchellurgero/org.urgero.akaunting:latest

## Install app
cloudron install --image=mitchellurgero/org.urgero.akaunting:latest
