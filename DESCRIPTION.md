## Akaunting

> Akaunting is a free, open source and online accounting software designed for small businesses and freelancers. It is built with modern technologies such as Laravel, VueJS, Bootstrap 4, RESTful API etc. Thanks to its modular structure, Akaunting provides an awesome App Store for users and developers.

~ Akaunting GitHub

### Cron

This app supports running one or more cronjobs. The jobs are specified using the standard crontab syntax.

## ionCube

ionCube is a PHP module extension that loads encrypted PHP files and speeds up webpages. ionCube is pre-installed
and enabled by default.

### Remote Terminal

Use the [web terminal](https://cloudron.io/documentation/apps/#web-terminal) for a remote shell connection into the
app to adjust configuration files like `php.ini`.

