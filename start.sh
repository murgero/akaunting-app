#!/bin/bash

set -eu

mkdir -p /app/data/public /run/apache2 /run/cron /run/app/sessions

if [[ ! -f /app/data/.installed ]]; then
    echo -e "==> First run check"
    cd /app/data/public && php artisan install --db-host="${CLOUDRON_MYSQL_HOST}" --db-name="${CLOUDRON_MYSQL_DATABASE}" --db-username="${CLOUDRON_MYSQL_USERNAME}" --db-password="${CLOUDRON_MYSQL_PASSWORD}" --admin-email="admin@example.com" --admin-password="changeme"
    chown -R www-data:www-data /app/data
    touch /app/data/.installed
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

# source it so that env vars are persisted
echo "==> Source custom startup script"
[[ -f /app/data/run.sh ]] && source /app/data/run.sh

[[ ! -f /app/data/crontab ]] && cp /app/code/crontab.template /app/data/crontab

## configure in-container Crontab
# http://www.gsp.com/cgi-bin/man.cgi?section=5&topic=crontab
if ! (env; cat /app/data/crontab; echo -e '\nMAILTO=""') | crontab -u www-data -; then
    echo "==> Error importing crontab. Continuing anyway"
else
    echo "==> Imported crontab"
fi

# phpMyAdmin auth file
if [[ ! -f /app/data/.phpmyadminauth ]]; then
    echo "==> Generating phpMyAdmin authentication file"
    PASSWORD=`pwgen -1 16`
    htpasswd -cb /app/data/.phpmyadminauth admin "${PASSWORD}"
    sed -e "s,PASSWORD,${PASSWORD}," /app/code/phpmyadmin_login.template > /app/data/phpmyadmin_login.txt
fi

echo "==> Creating credentials.txt"
sed -e "s,\bMYSQL_HOST\b,${CLOUDRON_MYSQL_HOST}," \
    -e "s,\bMYSQL_PORT\b,${CLOUDRON_MYSQL_PORT}," \
    -e "s,\bMYSQL_USERNAME\b,${CLOUDRON_MYSQL_USERNAME}," \
    -e "s,\bMYSQL_PASSWORD\b,${CLOUDRON_MYSQL_PASSWORD}," \
    -e "s,\bMYSQL_DATABASE\b,${CLOUDRON_MYSQL_DATABASE}," \
    -e "s,\bMYSQL_URL\b,${CLOUDRON_MYSQL_URL}," \
    -e "s,\bMAIL_SMTP_SERVER\b,${CLOUDRON_MAIL_SMTP_SERVER}," \
    -e "s,\bMAIL_SMTP_PORT\b,${CLOUDRON_MAIL_SMTP_PORT}," \
    -e "s,\bMAIL_SMTPS_PORT\b,${CLOUDRON_MAIL_SMTPS_PORT}," \
    -e "s,\bMAIL_SMTP_USERNAME\b,${CLOUDRON_MAIL_SMTP_USERNAME}," \
    -e "s,\bMAIL_SMTP_PASSWORD\b,${CLOUDRON_MAIL_SMTP_PASSWORD}," \
    -e "s,\bMAIL_FROM\b,${CLOUDRON_MAIL_FROM}," \
    -e "s,\bMAIL_DOMAIN\b,${CLOUDRON_MAIL_DOMAIN}," \
    -e "s,\bREDIS_HOST\b,${CLOUDRON_REDIS_HOST}," \
    -e "s,\bREDIS_PORT\b,${CLOUDRON_REDIS_PORT}," \
    -e "s,\bREDIS_PASSWORD\b,${CLOUDRON_REDIS_PASSWORD}," \
    -e "s,\bREDIS_URL\b,${CLOUDRON_REDIS_URL}," \
    /app/code/credentials.template > /app/data/credentials.txt

chown -R www-data:www-data /app/data /run/apache2 /run/app /tmp

echo "==> Starting Lamp stack"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Lamp
